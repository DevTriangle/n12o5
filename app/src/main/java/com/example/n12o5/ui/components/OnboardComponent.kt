package com.example.n12o5.ui.components

import android.widget.Space
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Paint
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.n12o5.ui.theme.Lato
import com.example.n12o5.ui.theme.captionColor
import com.example.n12o5.ui.theme.onboardTitle
import com.example.n12o5.ui.theme.primaryVariant
import com.example.n12o5.R

/*
Описание: Компонент приветственного экрана
Дата создания: 30.03.2023
Автор: Хасанов Альберт
 */
@Composable
fun OnboardComponent(
    info: Map<String, Any>,
    currentIndex: Int
) {
    Column(modifier = Modifier
        .widthIn(max = 440.dp)
        .padding(horizontal = 20.dp)
        .fillMaxSize(),
        verticalArrangement = Arrangement.SpaceBetween,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Column(
            modifier = Modifier
                .widthIn(max = 440.dp)
                .fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Spacer(modifier = Modifier.height(60.dp))
            Column(
                modifier = Modifier
                    .widthIn(max = 250.dp)
                    .height(120.dp)
                    .fillMaxWidth(),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Text(
                    text = info["title"].toString(),
                    fontSize = 20.sp,
                    fontWeight = FontWeight.Bold,
                    fontFamily = Lato,
                    color = onboardTitle,
                    textAlign = TextAlign.Center,
                    modifier = Modifier
                        .fillMaxWidth()
                )
                Spacer(modifier = Modifier.height(29.dp))
                Text(
                    text = info["desc"].toString(),
                    fontSize = 14.sp,
                    color = captionColor,
                    textAlign = TextAlign.Center,
                    modifier = Modifier
                        .fillMaxWidth()
                )
            }
            Spacer(modifier = Modifier.height(60.dp))
            DotsIndicator(currentIndex = currentIndex, count = 3)
        }
        Image(
            painter = info["image"] as Painter,
            contentDescription = "",
            modifier = Modifier
                .padding(bottom = 55.dp)
                .height(240.dp),
            contentScale = ContentScale.FillHeight
        )
    }
}

/*
Описание: Индикатор номера экрана
Дата создания: 30.03.2023
Автор: Хасанов Альберт
 */
@Composable
fun DotsIndicator(
    currentIndex: Int,
    count: Int
) {
    Row {
        for (i in 0 until count) {
            Box(
                modifier = Modifier
                    .padding(4.dp)
                    .size(14.dp)
                    .clip(CircleShape)
                    .border(1.dp, primaryVariant, CircleShape)
                    .background(if (currentIndex == i) primaryVariant else Color.White)
            )
        }
    }
}