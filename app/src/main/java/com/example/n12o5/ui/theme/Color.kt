package com.example.n12o5.ui.theme

import androidx.compose.ui.graphics.Color

val primary = Color(0xFF1A6FEE)
val primaryVariant = Color(0xFF57A9FF)
val onboardTitle = Color(0xFF00B712)
val captionColor = Color(0xFF939396)
val description = Color(0xFF7E7E9A)
val inputBG = Color(0xFFF5F5F9)
val inputStroke = Color(0xFFEBEBEB)