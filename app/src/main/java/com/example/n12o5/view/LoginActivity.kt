package com.example.n12o5.view

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.widget.Space
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.max
import androidx.compose.ui.unit.sp
import androidx.lifecycle.ViewModelProvider
import com.example.n12o5.ui.theme.N12o5Theme
import com.example.n12o5.R
import com.example.n12o5.ui.components.AppButton
import com.example.n12o5.ui.components.AppTextButton
import com.example.n12o5.ui.components.AppTextField
import com.example.n12o5.ui.components.LoadingDialog
import com.example.n12o5.ui.theme.captionColor
import com.example.n12o5.ui.theme.description
import com.example.n12o5.ui.theme.inputStroke
import com.example.n12o5.viewmodel.LoginViewModel

/*
Описание: Активити экрана авторизации
Дата создания: 30.03.2023
Автор: Хасанов Альберт
 */
class LoginActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            N12o5Theme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    ScreenContent()
                }
            }
        }
    }

    /*
    Описание: Содрежание экрана авторизации
    Дата создания: 30.03.2023
    Автор: Хасанов Альберт
     */
    @Composable
    fun ScreenContent() {
        val mContext = LocalContext.current
        val viewModel = ViewModelProvider(this)[LoginViewModel::class.java]

        var email by rememberSaveable { mutableStateOf("") }

        var isLoading by rememberSaveable { mutableStateOf(false) }
        var isErrorVisible by rememberSaveable { mutableStateOf(false) }

        val isSuccess by viewModel.isSuccess.observeAsState()
        LaunchedEffect(isSuccess) {
            if (isSuccess == true) {
                isLoading = false

                val intent = Intent(mContext, CodeActivity::class.java)
                intent.putExtra("email", email)
                startActivity(intent)
            }
        }

        val message by viewModel.message.observeAsState()
        LaunchedEffect(message) {
            if (message != null) {
                isLoading = false
                isErrorVisible = true
            }
        }

        Column(
            Modifier
                .widthIn(max = 400.dp)
                .padding(horizontal = 20.dp)
                .fillMaxSize()
                .verticalScroll(rememberScrollState()),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.SpaceBetween
        ) {
            Column(
                Modifier
                    .widthIn(max = 400.dp)
                    .fillMaxSize(),
            ) {
                Spacer(modifier = Modifier.height(60.dp))
                Row(
                    modifier = Modifier
                        .widthIn(max = 400.dp)
                        .fillMaxWidth(),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Image(
                        painter = painterResource(id = R.drawable.ic_hello),
                        contentDescription = "",
                        modifier = Modifier.size(32.dp)
                    )
                    Spacer(modifier = Modifier.width(16.dp))
                    Text(
                        text = "Добро пожаловать!",
                        fontSize = 24.sp,
                        fontWeight = FontWeight.Bold
                    )
                }
                Spacer(modifier = Modifier.height(23.dp))
                Text(
                    text = "Войдите, чтобы пользоваться функциями приложения",
                    fontSize = 15.sp
                )
                Spacer(modifier = Modifier.height(61.dp))
                AppTextField(
                    value = email,
                    onValueChange = {
                        email = it
                    },
                    label = {
                        Text(
                            text = "Вход по E-mail",
                            color = description,
                            fontSize = 14.sp
                        )
                    },
                    placeholder = {
                        Text(
                            text = "example@mail.ru",
                            color = Color.Black.copy(0.5f),
                            fontSize = 15.sp
                        )
                    },
                    modifier = Modifier
                        .fillMaxWidth()
                )
                Spacer(modifier = Modifier.height(32.dp))
                AppButton(
                    onClick = {
                        if (Regex("^[a-zA-Z0-9]*@[a-zA-Z0-9]*\\.[a-zA-Z0-9]{2,}$").matches(email)) {
                            isLoading = true
                            viewModel.sendCode(email)
                        } else {
                            Toast.makeText(mContext, "Неверный формат E-Mail", Toast.LENGTH_LONG).show()
                        }
                    },
                    label = "Далее",
                    enabled = email.isNotBlank(),
                    textStyle = TextStyle(fontWeight = FontWeight.SemiBold)
                )
            }
            Column(
                Modifier
                    .widthIn(max = 400.dp)
                    .fillMaxWidth(),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Spacer(modifier = Modifier.height(30.dp))
                Text(
                    text = "Или войдите с помощью",
                    fontSize = 15.sp,
                    color = captionColor,
                    textAlign = TextAlign.Center
                )
                Spacer(modifier = Modifier.height(16.dp))
                AppButton(
                    onClick = {},
                    label = "Войти с Яндекс",
                    colors = ButtonDefaults.buttonColors(
                        backgroundColor = Color.White,
                        contentColor = Color.Black
                    ),
                    border = BorderStroke(1.dp, inputStroke)
                )
                Spacer(modifier = Modifier.height(55.dp))
            }
        }

        if (isLoading) {
            LoadingDialog()
        }

        if (isErrorVisible) {
            AlertDialog(
                onDismissRequest = { isErrorVisible = false },
                title = {
                    Text(text = "Ошибка", fontSize = 18.sp, fontWeight = FontWeight.SemiBold)
                },
                text = {
                    Text(text = message.toString(), fontSize = 16.sp)
                },
                buttons = {
                    AppTextButton(onClick = { isErrorVisible = false }, label = "Ок")
                }
            )
        }
    }
}