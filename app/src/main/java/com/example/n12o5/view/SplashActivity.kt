package com.example.n12o5.view

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import com.example.n12o5.R
import com.example.n12o5.ui.theme.N12o5Theme
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

/*
Описание: Активити Launch-экрана
Дата создания: 30.03.2023
Автор: Хасанов Альберт
 */
@SuppressLint("CustomSplashScreen")
class SplashActivity : ComponentActivity() {
    @SuppressLint("CoroutineCreationDuringComposition")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val mContext = LocalContext.current
            val sharedPreferences = this.getSharedPreferences("shared", MODE_PRIVATE)
            val scope = rememberCoroutineScope()

            val isFirstLaunch = sharedPreferences.getBoolean("isFirstLaunch", true)
            val token = sharedPreferences.getString("token", "")


            N12o5Theme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    ScreenContent()

                    scope.launch {
                        delay(1000)

                        if (isFirstLaunch) {
                            val intent = Intent(mContext, OnboardActivity::class.java)
                            startActivity(intent)
                        } else {
                            if (token != "") {
                                val intent = Intent(mContext, PasswordActivity::class.java)
                                startActivity(intent)
                            } else {
                                val intent = Intent(mContext, LoginActivity::class.java)
                                startActivity(intent)
                            }
                        }
                    }
                }
            }
        }
    }

    /*
    Описание: Активити Launch-экрана
    Дата создания: 30.03.2023
    Автор: Хасанов Альберт
     */
    @Composable
    fun ScreenContent() {
        Image(
            painter = painterResource(id = R.drawable.splash_bg),
            contentDescription = "",
            modifier = Modifier.fillMaxSize(),
            contentScale = ContentScale.FillWidth
        )

        Box(modifier = Modifier.fillMaxSize()) {
            Image(
                painter = painterResource(id = R.drawable.logo),
                contentDescription = "",
                modifier = Modifier.align(Alignment.Center)
            )
        }
    }
}