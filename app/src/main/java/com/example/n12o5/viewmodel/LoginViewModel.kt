package com.example.n12o5.viewmodel

import android.provider.ContactsContract.CommonDataKinds.Email
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.n12o5.common.ApiService
import kotlinx.coroutines.launch

/*
Описание: ViewModel для авторизации пользователя
Дата создания: 30.03.2023
Автор: Хасанов Альберт
*/
class LoginViewModel: ViewModel() {
    val message = MutableLiveData<String>()
    val token = MutableLiveData<String>()
    val isSuccess = MutableLiveData<Boolean>()

    /*
    Описание: Метод для отправки кода на email
    Дата создания: 30.03.2023
    Автор: Хасанов Альберт
    */
    fun sendCode(email: String) {
        message.value = null
        isSuccess.value = null

        val apiService = ApiService.getInstance()

        viewModelScope.launch {
            try {
                val json = apiService.sendCode(email)

                if (json.code() == 200) {
                    isSuccess.value = true
                } else {
                    isSuccess.value = false
                    message.value = "${json.code()} ${json.body()?.get("errors")}"
                }
            } catch (e: Exception) {
                isSuccess.value = false
                message.value = e.message
            }
        }
    }

    /*
    Описание: Метод для авторизации пользователя
    Дата создания: 30.03.2023
    Автор: Хасанов Альберт
    */
    fun signIn(email: String, code: String) {
        message.value = null
        token.value = null

        val apiService = ApiService.getInstance()

        viewModelScope.launch {
            try {
                val json = apiService.signIn(email, code)

                if (json.code() == 200) {
                    token.value = json.body()?.get("token").toString()
                } else {
                    message.value = "${json.code()} ${json.body()?.get("errors")}"
                }
            } catch (e: Exception) {
                message.value = e.message
            }
        }
    }
}